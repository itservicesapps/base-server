# USERS
user { 'jq':
    ensure      => 'present',
    groups      => 'wheel',
    managehome  => true,
    shell       => '/bin/zsh',
    require     => Package['zsh'],
}
file { '/home/jq/.zsh':
    ensure      => directory,
    owner       => 'jq',
    group       => 'jq',
    require     => User['jq']
}
file { '/home/jq/.zsh/git-prompt':
    ensure      => directory,
    owner       => 'jq',
    group       => 'jq',
    require     => File['/home/jq/.zsh'],
}
file { '/home/jq/.zsh/git-prompt/gitstatus.py':
    ensure      => 'present',
    owner       => 'jq',
    group       => 'jq',
    content     => file('/tmp/puppet/files/jq/gitstatus.py'),
    require     => File['/home/jq/.zsh/git-prompt'],
}
file { '/home/jq/.zsh/git-prompt/zshrc.sh':
    ensure      => 'present',
    owner       => 'jq',
    group       => 'jq',
    content     => file('/tmp/puppet/files/jq/zshrc.sh'),
    require     => File['/home/jq/.zsh/git-prompt'],
}
file { '/home/jq/.zprofile':
    ensure      => 'present',
    owner       => 'jq',
    group       => 'jq',
    content     => file('/tmp/puppet/files/jq/.zprofile'),
}
file { '/home/jq/.ssh':
    ensure      => directory,
    owner       => 'jq',
    group       => 'jq',
    mode        => '0700',
    require     => User['jq'],
}
file { '/home/jq/.ssh/authorized_keys':
    ensure      => present,
    owner       => 'jq',
    group       => 'jq',
    mode        => '0600',
    content     => file('/tmp/puppet/files/keys/jq'),
    require     => User['jq'],
}
file { '/home/jq/.zshrc':
    ensure      => present,
    owner       => 'jq',
    group       => 'jq',
    content     => file('/tmp/puppet/files/jq/.zshrc'),
    require     => User['jq'],
}
file { '/home/jq/.vimrc':
    ensure      => present,
    owner       => 'jq',
    group       => 'jq',
    content     => file('/tmp/puppet/files/jq/.vimrc'),
    require     => User['jq'],
}

# PACKAGES

package { 'mosh': }
package { 'zsh': }

# CLASSES
include augeas
include sudo

class { 'ssh::server':
    require => Class['augeas'],
}

class { '::ntp':
    servers => [ 'pool.ntp.org'],
}

class { 'nginx': }


# CONFIGURATIONS

ssh::server::configline { 'PermitRootLogin': value => 'no' }
ssh::server::configline { 'PasswordAuthentication': value => 'no' }
ssh::server::configline { 'AllowUsers/1': value => 'jq' }

sudo::conf { 'sudo':
    priority    => 10,
    content     => "%sudo ALL=(ALL) NOPASSWD: ALL\n",
}

#jeffreyquinn.com content

# FILES

